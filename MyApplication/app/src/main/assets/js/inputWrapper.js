var elementUpdateCallback = null;
var webWidth = null;
var webHeight = null;

/**
 * The java code handles the inputTopic life-cycle. 
 * Here we just return what is needed to allow the assist-input-manager.js 
 * to behave how Android requires.
 */
var assistSDKInterface = {
    
    hasInputTopic : function() {
        console.log("hasInputTopic");
        return false;
    },
    inputTopicHasAgent : function() {
        return false;
    },
    openInputTopic : function(agentJoined, callback) {
        agentJoined();
        elementUpdateCallback = callback;
    },
    noInputs : function() {
        inputElementsCallback.onNoInputElements();
        console.log("noInputs");
    }
};

window.onunload = pageUnloaded;

function pageUnloaded()
{
    console.log("pageUnloaded");
    inputElementsCallback.onPageUnloaded();
}

function updateElements(descriptors) {
    elementUpdateCallback(descriptors);
}

function setUpInputListener() {
    window.AssistInputManager.init(assistSDKInterface, onInputElementChanged,
            onInputElementClicked, onNewInputElements, hasPermissionToInteract);
    startInputElementObserver();
}

function onInputElementChanged(elementDescriptor) {
    console.log("onInputElementChanged(" + elementDescriptor);
    inputElementsCallback.on(elementDescriptor);
}

function onInputElementClicked(descString, boundsLeft, boundsTop) {
    var viewPortWidth = window.innerWidth;
    var viewPortHeight = window.innerHeight;

    var xScale = viewPortWidth / webWidth;
    var yScale = viewPortHeight / webHeight;

    var androidXFixOffset = getAndroidXOffsetFix();
    var fixedX = boundsLeft - androidXFixOffset;
    var scaledX = fixedX / xScale;

    /** hack for android bug where viewport y axis is reported incorrectly */
    var androidYFixOffset = getAndroidYOffsetFix();
    var fixedY = boundsTop - androidYFixOffset;
    var scaledY = fixedY / yScale;

    inputElementsCallback.onInputElementClicked(descString, scaledX, scaledY);
}

/** Android has a bug whereby the viewport Y axis is not accurately reported
 *  The following works out how far it is out.
 */
function getAndroidYOffsetFix() {
    var bodyClientTop = document.body.parentElement.getBoundingClientRect().top;
    var offsetY = bodyClientTop + window.pageYOffset;
    return offsetY;
}

/** Android has a bug whereby the viewport X axis is not accurately reported
 * The following works out how far it is out.
 */
function getAndroidXOffsetFix() {
    var bodyClientLeft = document.body.parentElement.getBoundingClientRect().left;
    var offsetX = bodyClientLeft + window.pageXOffset;
    return offsetX;
}

function onNewInputElements(elementDescriptors) {
    inputElementsCallback.onNewInputElements(elementDescriptors);
}

/**
 * for now, we permit everyone.
 * @param element dom element to inspect for permissions.
 * @param permissionDefinitions available permissions.
 * @returns {boolean} true is allowed to interact.
 */
function hasPermissionToInteract(element, permissionDefinitions) {
    return true;
}

/** Translates the x and y from the Android coordinate space to the
 *  web page coordinate space
 */
function translateXandY(x, y, webViewWidth, webViewHeight) {
    var viewPortWidth = document.documentElement.clientWidth;
    var viewPortHeight = document.documentElement.clientHeight;

    var xScale = viewPortWidth / webViewWidth;
    var yScale = viewPortHeight / webViewHeight;

    var scaledX = x * xScale;
    var scaledY = y * yScale;

    /** hack for android bug where viewport XY axis is reported incorrectly */
    var androidYFixOffset = getAndroidYOffsetFix();
    var androidXFixOffset = getAndroidXOffsetFix();
    console.log("x offset=" + androidXFixOffset + ", y offset="
            + androidYFixOffset);

    var translatedPoint = {
        x : scaledX + androidXFixOffset,
        y : scaledY + androidYFixOffset
    };

    return translatedPoint;
}

var observer = null;

/** Starts the input Element observer which checks for when input elements are
 *  added or removed.
 */
function startInputElementObserver() {
    var config = {
        attributes : true,
        childList : true,
        characterData : true,
        subtree : true
    };

    if (('MutationObserver' in window) && (observer == null || typeof observer === 'undefined')) {
        console.log("init observer");
        var recheckForms = false;
        observer = new MutationObserver(function(mutations) {
            var rerender = true;
            for (var i = 0; i < mutations.length; i++) {
                var mutation = mutations[i];

                for (var j = 0; (j < mutation.addedNodes.length)
                        && !recheckForms; j++) {
                    if (isFormElement(mutation.addedNodes.item(j))) {
                        console.log("input elment added to DOM");
                        recheckForms = true;
                    }
                }
                for (var j = 0; (j < mutation.removedNodes.length)
                        && !recheckForms; j++) {
                    if (isFormElement(mutation.removedNodes.item(j))) {
                        console.log("input elment removed from DOM");
                        recheckForms = true;
                    }
                }
            }

            if (rerender == true) {
                if (recheckForms) {
                    window.AssistInputManager.mapInputElements();
                }
            }
        });
    }

    console.log("scheduling observer");
    /** pass in the target node, as well as the observer options */
    try {
        observer.observe(window.document.body, config);
    } catch (e) {
        console.log("ERROR_OBSERVER_FAILED " + e);
    }
}

function isFormElement(element) {
    return (element.tagName == "INPUT" || element.tagName == "SELECT" || element.tagName == "TEXTAREA");
}

function simulateMouseEvent(x, y, webViewWidth, webViewHeight, assistSource) {
    webWidth = webViewWidth;
    webHeight = webViewHeight;

    var p2 = translateXandY(x, y, webViewWidth, webViewHeight);
    var element = document.elementFromPoint(p2.x, p2.y);

    console.log("element on MOUSE_UP " + element);

    if ((element != null) && (element != undefined)) {
        var event;
        if (typeof _cafex_lastElement !== 'undefined'
                && _cafex_lastElement != null && _cafex_lastElement != element) {
            event = document.createEvent('MouseEvents');
            event.initEvent('mouseout', true, true);
            event.assist_generated = true;
            event.assist_source = assistSource;
            _cafex_lastElement.dispatchEvent(event);
        }
        if (typeof _cafex_lastElement === 'undefined'
                || _cafex_lastElement == null || _cafex_lastElement != element) {
            event = document.createEvent('MouseEvents');
            event.initMouseEvent('mouseover', true, true, window, 1, p2.x,
                    p2.y, p2.x, p2.y, false, false, false, false, 0, null);
            event.assist_generated = true;
            event.assist_source = assistSource;
            element.dispatchEvent(event);
        }
        event = document.createEvent('MouseEvents');
        event.initMouseEvent('mousemove', true, true, window, 1, p2.x, p2.y,
                p2.x, p2.y, false, false, false, false, 0, null);
        event.assist_generated = true;
        event.assist_source = assistSource;
        element.dispatchEvent(event);
        event = document.createEvent('MouseEvents');
        event.initMouseEvent('mousedown', true, true, window, 1, p2.x, p2.y,
                p2.x, p2.y, false, false, false, false, 0, null);
        event.assist_generated = true;
        element.dispatchEvent(event);
        event = document.createEvent('MouseEvents');
        event.initMouseEvent('mouseup', true, true, window, 1, p2.x, p2.y,
                p2.x, p2.y, false, false, false, false, 0, null);
        event.assist_generated = true;
        event.assist_source = assistSource;
        element.dispatchEvent(event);

        event = document.createEvent('MouseEvents');
        event.initMouseEvent('click', true, true, window, 1, p2.x, p2.y, p2.x,
                p2.y, false, false, false, false, 0, null);
        event.assist_generated = true;
        event.assist_source = assistSource;
        element.dispatchEvent(event);
    }
    _cafex_lastElement = element;
}

function getInputElementsWhenLoaded() {
    if (document.readyState === "complete") {
        setUpInputListener();
        window.AssistInputManager.mapInputElements();
    } else {
        setUpInputListener();
        window.addEventListener("onload", function() {
            window.AssistInputManager.mapInputElements();
        }, false);
    }
}